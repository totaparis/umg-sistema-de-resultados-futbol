var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        var key = decodeURIComponent(sParameterName[0]);
        var value = decodeURIComponent(sParameterName[1]);

        if (key === sParam) {
            return value === undefined ? true : value;
        }
    }
};

let ID = getUrlParameter('idPartido');

console.log(ID);
$.ajax({
    dataType: "json",
    //url: 'https://sistema-fut.s3.amazonaws.com/frontend/files/estadistica.json',
    //url: 'http://127.0.0.1:5500/frontend/files/estadistica.json',
    url: 'https://umgweb.000webhostapp.com/api/ruta.php?api=1&idPartido='+ID,
    success: function (response){
        processData(response.data,response.fechaDelPartido);
    },
    error: function () {
        console.log("No se ha podido obtener la información");
    }
  });

  function processData(data,date){
    console.log(data);
     
    setTeamName(data,date);
    setGolPlayers(data);
    setMetrics(data);

}

function setTeamName(data,date){
    let template = `
    <div class="">Liga de Guatemala: ${date.substring(0,11)}</div>
    <div class="row">
        <div class="item-row">${data[0].nombreEquipo}</div>
        <div class="item-row">${data[0].Goles}</div>
        <div class="item-row">-</div>
        <div class="item-row">${data[1].Goles}</div>
        <div class="item-row">${data[1].nombreEquipo}</div>
    </div>
    `
    $('#teamName').append(template);
}
function setGolPlayers(data){

    let template = `
    <div id="players" class="row">
        <div>
    `
    $.each(data[0].jugadorGol, function( index, value ) {
        template += `<div><p>${value}<p></div>`
    });
    template += `</div>`
    template += `<div>GOL</div>`
    template += `<div>`
    $.each(data[1].jugadorGol, function( index, value ) {
        template += `<div><p>${value}<p></div>`
    });
    template += `</div>` 

    template += `</div>`

    $('#teamName').append(template);

    
}
function setMetrics(data){
    let template = `
    <div class="row">
        <div>${data[0].remate}</div>
        <div>Remates</div>
        <div>${data[1].remate}</div>
    </div>
    <div class="row">
        <div>${data[0].remateaarco}</div>
        <div>Remates al Arco</div>
        <div>${data[1].remateaarco}</div>
    </div>
    <div class="row">
        <div>${data[0].posesion}</div>
        <div>Posesion</div>
        <div>${data[1].posesion}</div>
    </div>
    <div class="row">
        <div>${data[0].pase}</div>
        <div>Pase</div>
        <div>${data[1].pase}</div>
    </div>
    <div class="row">
        <div>${data[0].presisionpase}</div>
        <div>Presision Pase</div>
        <div>${data[1].presisionpase}</div>
    </div>
    <div class="row">
        <div>${data[0].falta}</div>
        <div>Falta</div>
        <div>${data[1].falta}</div>
    </div>
    <div class="row">
        <div>${data[0].posisionadelantada}</div>
        <div>Posicion Adelantada</div>
        <div>${data[1].posisionadelantada}</div>
    </div>
    <div class="row">
        <div>${data[0].tirosesquina}</div>
        <div>Tiros de Esquina</div>
        <div>${data[1].tirosesquina}</div>
    </div>
    <div class="row">
        <div>${data[0].TarjetaAmarilla}</div>
        <div>Tarjetas Amarilla</div>
        <div>${data[1].TarjetaAmarilla}</div>
    </div>
    <div class="row">
        <div>${data[0].TarjetaRoja}</div>
        <div>Tarjeta Roja</div>
        <div>${data[1].TarjetaRoja}</div>
    </div>
    `
    $('#metric').append(template);
} 