class barraNavegacion extends HTMLElement{
    constructor(){
        super();
        let template = document.getElementById('barra-navegacion');
        let shadow = this.attachShadow({mode: 'open'})
        shadow.appendChild(template.content.cloneNode(true));


        let  ulNav = document.createElement("ul");
        ulNav.setAttribute("class","navigation");

        let liPartido = document.createElement("li");
        let liPos = document.createElement("li");

        let aPartido = document.createElement("a");
        let aPos = document.createElement("a");

        aPartido.textContent = "Partidos";
        aPos.textContent = "Posiciones";

        aPartido.setAttribute("href","./partidos.html");
        aPos.setAttribute("href","./posiciones.html");

        shadow.appendChild(ulNav);
        ulNav.appendChild(liPartido);
        liPartido.appendChild(aPartido);

        ulNav.appendChild(liPos);
        liPos.appendChild(aPos);

    }
}
customElements.define("barra-navegacion", barraNavegacion);