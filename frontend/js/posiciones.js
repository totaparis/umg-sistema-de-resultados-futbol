$.ajax({
    dataType: "json",
    //url: 'https://sistema-fut.s3.amazonaws.com/frontend/files/posiciones.json',
    //url: 'http://127.0.0.1:5500/frontend/files/posiciones.json',
    url: 'https://umgweb.000webhostapp.com/api/ruta.php?api=3',
    method: "GET",
    success: function (response){
        processData(response.data,response.temporada);
    },
    error: function () {
        console.log("No se ha podido obtener la información");
    }
  });

  function processData(data,date){
    console.log(data);
     
    setTitle(date);
    setTableTeams(data);
}

function setTitle(date){
    let template = `<h3>Temporada: 2019-20</h3>`
    
    $('#title').append(template);
}

function setTableTeams(data,date){
    let template = ``
    

    $.each(data, function( index, value ) {
        template += `
        <tr>
        <td>${data[index].posicion}</td>
        <td>${data[index].nombreEquipo}</td>
        <td>${data[index].pj}</td>
        <td>${data[index].g}</td>
        <td>${data[index].e}</td>
        <td>${data[index].p}</td>
        <td>${data[index].gf}</td>
        <td>${data[index].gc}</td>
        <td>${data[index].pts}</td>
        </tr>
        `
    });

    $('#position').append(template);
}