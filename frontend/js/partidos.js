$.ajax({
    dataType: "json",
    //url: 'http://sistema-fut.s3.amazonaws.com/frontend/files/partidos.json',
    //url: 'http://127.0.0.1:5500/frontend/files/partidos.json',
    url: 'https://umgweb.000webhostapp.com/api/ruta.php?api=2',
    method:"GET",
    success: function (response){
        processData(response.data,response.temporada);
    },
    error: function () {
        console.log("No se ha podido obtener la información");
    }
  });

  function processData(data,date){
     console.log(data);
    setMatch(data);
}

function setMatch(data,date){
        let template = ``
    

    $.each(data, function( index, value ) {
        // console.log(data[index].jornada);
        // console.log(data[index].partidos);
        template += `
        <div class="partidos">
            <div class="jornada">
                <h3>Jornada: ${data[index].jornada}</h3>
            </div>
            <div class="p-container">
        `
        $.each(data[index].partidos, function( index, value ) {
            template += `            
            
                <div class="p-item">
                    <a href="./estadistica.html?idPartido=${value.idPartido}">
                        <p>${value.idPartido}</p>
                            <div>
                                <table>
                                    <tr>
                                        <td>${value.equipo[0].nombreEquipo}</td>
                                        <td>${value.equipo[0].cantidadGol}</td> 
                                    </tr>
                                    <tr>
                                        <td>${value.equipo[1].nombreEquipo}</td>
                                        <td>${value.equipo[1].cantidadGol}</td>
                                    </tr>
                                </table>
                            </div>
                        <p>${value.fecha.substring(0,11)}</p>
                    <a>
                </div>
            
            `
            // console.log(value.fecha);
            //  console.log(value.equipo[0].nombreEquipo);
            //  console.log(value.equipo[0].cantidadGol);
        });
        template += `</div>`;
        template += `</div>`;
        
    });
    

    $('#partidos').append(template);
}