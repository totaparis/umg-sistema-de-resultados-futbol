
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadio`
--

DROP TABLE IF EXISTS `estadio`;
CREATE TABLE IF NOT EXISTS `estadio` (
  `idestadio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `ubicacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idestadio`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estadio`
--

INSERT INTO `estadio` (`idestadio`, `nombre`, `ubicacion`) VALUES
(1, 'Estadio Doroteo Guamuch Flores', 'Ciudad de Guatemala'),
(2, 'Israel Barrios', 'Coatepeque'),
(3, 'Cementos Progreso', 'Ciudad de Guatemala'),
(4, 'Winston Pineda', 'Jutiapa'),
(5, 'Estadio José Ángel Rossi', 'Cobán'),
(6, 'Estadio Ricardo Muñoz Gálvez', 'Santa Lucía Cotzumalguapa'),
(7, 'Estadio Santa Lucía', 'San Marcos'),
(8, 'Estadio del Ejército', 'Ciudad de Guatemala'),
(9, 'Estadio Guillermo Slowing', 'Amatitlán'),
(10, 'Estadio Mario Camposeco', 'Quetzaltenango'),
(11, 'Estadio Armando Barillas', 'Escuintla'),
(12, 'Estadio Carlos Salazar Hijo', 'Suchitepéquez'),
(13, 'Estadio Pensativo', 'Antigua Guatemala'),
(14, 'Estadio Las Flores', 'Jalapa'),
(15, 'Estadio Municipal Mateo Sicay Paz', 'Siquinalá'),
(16, 'Estadio David Cordón Hichos', 'Guastatoya'),
(17, 'Estadio Municipal de Sanarate', 'Sanarate'),
(18, 'Estadio Santo Domingo', 'Mixco'),
(19, 'Estadio Municipal El Morón', 'Iztapa'),
(20, 'Estadio Manuel Felipe Carrera', 'Ciudad de Guatemala'),
(21, 'Estadio Municipal Santa Lucía Cotzumalguapa', 'Santa Lucía Cotzumalguapa');
