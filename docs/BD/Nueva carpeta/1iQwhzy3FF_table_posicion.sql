
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posicion`
--

DROP TABLE IF EXISTS `posicion`;
CREATE TABLE IF NOT EXISTS `posicion` (
  `idposicion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idposicion`),
  UNIQUE KEY `idposicion_UNIQUE` (`idposicion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `posicion`
--

INSERT INTO `posicion` (`idposicion`, `descripcion`) VALUES
(1, 'Defensa'),
(2, 'Delantero'),
(3, 'Medio'),
(4, 'Portero');
