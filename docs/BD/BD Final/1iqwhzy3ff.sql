-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-09-2019 a las 05:55:08
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `1iqwhzy3ff`
--
CREATE DATABASE IF NOT EXISTS `1iqwhzy3ff` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `1iqwhzy3ff`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificacion`
--

DROP TABLE IF EXISTS `calificacion`;
CREATE TABLE `calificacion` (
  `idcalificacion` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `calificacion`:
--

--
-- Volcado de datos para la tabla `calificacion`
--

INSERT INTO `calificacion` (`idcalificacion`, `descripcion`) VALUES
(1, 'Gol'),
(2, 'Tarjeta Amarilla'),
(3, 'Tarjeta Roja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_equipo`
--

DROP TABLE IF EXISTS `detalle_equipo`;
CREATE TABLE `detalle_equipo` (
  `iddetalle_equipo` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `puntoS` int(11) DEFAULT NULL,
  `jugado` int(11) DEFAULT NULL,
  `ganado` int(11) DEFAULT NULL,
  `empate` int(11) DEFAULT NULL,
  `perdido` int(11) DEFAULT NULL,
  `afavor` int(11) DEFAULT NULL,
  `encontra` int(11) DEFAULT NULL,
  `remate` int(11) DEFAULT NULL,
  `remateaarco` int(11) DEFAULT NULL,
  `posesion` int(11) DEFAULT NULL,
  `pase` int(11) DEFAULT NULL,
  `presisionpase` int(11) DEFAULT NULL,
  `falta` int(11) DEFAULT NULL,
  `posisionadelantada` int(11) DEFAULT NULL,
  `tirosesquina` int(11) DEFAULT NULL,
  `jornada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `detalle_equipo`:
--   `idequipo`
--       `equipo` -> `idequipo`
--

--
-- Volcado de datos para la tabla `detalle_equipo`
--

INSERT INTO `detalle_equipo` (`iddetalle_equipo`, `idequipo`, `puntoS`, `jugado`, `ganado`, `empate`, `perdido`, `afavor`, `encontra`, `remate`, `remateaarco`, `posesion`, `pase`, `presisionpase`, `falta`, `posisionadelantada`, `tirosesquina`, `jornada`) VALUES
(1, 1, 3, 1, 1, 0, 0, 2, 0, 8, 15, 47, 756, 51, 37, 12, 7, 1),
(2, 6, 3, 1, 1, 0, 0, 2, 0, 4, 10, 93, 610, 83, 17, 13, 10, 1),
(3, 11, 3, 1, 1, 0, 0, 2, 0, 9, 12, 56, 452, 40, 7, 11, 6, 1),
(4, 2, 3, 1, 1, 0, 0, 1, 0, 11, 10, 73, 424, 54, 28, 12, 7, 1),
(5, 4, 1, 1, 0, 1, 0, 1, 1, 13, 15, 83, 419, 64, 16, 13, 12, 1),
(6, 5, 1, 1, 0, 1, 0, 1, 1, 9, 10, 87, 651, 84, 15, 13, 15, 1),
(7, 10, 1, 1, 0, 1, 0, 0, 0, 14, 6, 60, 767, 56, 26, 5, 13, 1),
(8, 12, 1, 1, 0, 1, 0, 0, 0, 14, 14, 66, 737, 52, 35, 10, 11, 1),
(9, 3, 0, 1, 0, 0, 1, 0, 1, 13, 8, 55, 764, 75, 36, 15, 6, 1),
(10, 7, 0, 1, 0, 0, 1, 0, 2, 10, 5, 95, 460, 68, 30, 2, 11, 1),
(11, 8, 0, 1, 0, 0, 1, 0, 2, 10, 9, 56, 724, 61, 31, 9, 9, 1),
(12, 9, 0, 1, 0, 0, 1, 0, 2, 5, 12, 89, 720, 70, 14, 5, 12, 1),
(13, 1, 6, 2, 2, 0, 0, 3, 0, 13, 4, 63, 424, 58, 11, 14, 10, 2),
(14, 2, 6, 2, 2, 0, 0, 3, 1, 12, 12, 81, 783, 81, 13, 11, 11, 2),
(15, 6, 4, 2, 1, 1, 0, 3, 1, 10, 11, 74, 417, 40, 20, 5, 11, 2),
(16, 11, 4, 2, 1, 1, 0, 2, 0, 12, 11, 98, 450, 86, 27, 2, 15, 2),
(17, 10, 4, 2, 1, 1, 0, 1, 0, 6, 6, 85, 567, 72, 20, 10, 8, 2),
(18, 5, 2, 2, 0, 2, 0, 1, 1, 11, 5, 63, 601, 54, 39, 9, 10, 2),
(19, 12, 2, 2, 0, 2, 0, 0, 0, 7, 6, 88, 436, 54, 29, 8, 10, 2),
(20, 4, 1, 2, 0, 1, 1, 2, 3, 15, 7, 49, 508, 81, 18, 12, 15, 2),
(21, 9, 1, 2, 0, 1, 1, 1, 3, 5, 11, 80, 669, 63, 33, 13, 5, 2),
(22, 8, 1, 2, 0, 1, 1, 0, 2, 9, 5, 90, 717, 44, 14, 12, 9, 2),
(23, 3, 0, 2, 0, 0, 2, 0, 2, 9, 15, 71, 481, 43, 27, 3, 5, 2),
(24, 7, 0, 2, 0, 0, 2, 0, 3, 14, 5, 40, 477, 51, 9, 14, 13, 2),
(25, 1, 9, 3, 3, 0, 0, 6, 2, 4, 9, 95, 700, 72, 7, 2, 12, 3),
(26, 2, 9, 3, 3, 0, 0, 5, 1, 11, 15, 53, 648, 50, 17, 2, 7, 3),
(27, 10, 7, 3, 2, 1, 0, 2, 0, 5, 10, 61, 628, 87, 30, 2, 13, 3),
(28, 5, 5, 3, 1, 2, 0, 4, 3, 10, 11, 96, 534, 44, 29, 5, 12, 3),
(29, 12, 5, 3, 1, 2, 0, 3, 2, 5, 15, 96, 431, 88, 29, 9, 10, 3),
(30, 6, 4, 3, 1, 1, 1, 5, 4, 7, 13, 90, 553, 85, 9, 15, 15, 3),
(31, 11, 4, 3, 1, 1, 1, 2, 2, 15, 4, 53, 575, 59, 31, 4, 5, 3),
(32, 4, 2, 3, 0, 2, 1, 3, 4, 12, 11, 77, 439, 80, 36, 6, 11, 3),
(33, 3, 1, 3, 0, 1, 2, 1, 3, 10, 10, 79, 567, 67, 17, 11, 4, 3),
(34, 9, 1, 3, 0, 1, 2, 3, 6, 10, 7, 97, 573, 47, 17, 11, 15, 3),
(35, 8, 1, 3, 0, 1, 2, 0, 3, 9, 15, 54, 482, 42, 15, 3, 12, 3),
(36, 7, 0, 3, 0, 0, 3, 2, 6, 5, 7, 62, 582, 89, 7, 8, 15, 3),
(37, 2, 12, 4, 4, 0, 0, 8, 1, 7, 13, 60, 599, 60, 16, 6, 6, 4),
(38, 1, 10, 4, 3, 1, 0, 6, 2, 7, 13, 48, 643, 85, 21, 11, 10, 4),
(39, 12, 8, 4, 2, 2, 0, 5, 3, 5, 7, 50, 559, 66, 24, 14, 6, 4),
(40, 10, 7, 4, 2, 1, 1, 4, 3, 15, 6, 90, 699, 58, 36, 10, 4, 4),
(41, 5, 6, 4, 1, 3, 0, 4, 3, 8, 11, 61, 544, 81, 8, 3, 9, 4),
(42, 4, 5, 4, 1, 2, 1, 5, 4, 9, 14, 77, 747, 44, 25, 14, 5, 4),
(43, 3, 4, 4, 1, 1, 2, 4, 3, 15, 13, 85, 751, 73, 40, 11, 4, 4),
(44, 6, 4, 4, 1, 1, 2, 5, 7, 9, 15, 51, 613, 56, 27, 2, 9, 4),
(45, 11, 4, 4, 1, 1, 2, 2, 4, 15, 8, 93, 499, 46, 16, 4, 7, 4),
(46, 7, 3, 4, 1, 0, 3, 5, 8, 5, 9, 89, 687, 40, 30, 13, 15, 4),
(47, 9, 1, 4, 0, 1, 3, 4, 8, 15, 4, 42, 510, 44, 27, 12, 5, 4),
(48, 8, 1, 4, 0, 1, 3, 0, 6, 13, 5, 100, 622, 86, 5, 10, 15, 4),
(49, 1, 13, 5, 4, 1, 0, 8, 3, 12, 15, 83, 737, 89, 11, 2, 12, 5),
(50, 2, 12, 5, 4, 0, 1, 9, 3, 5, 7, 49, 542, 50, 8, 7, 15, 5),
(51, 10, 10, 5, 3, 1, 1, 5, 3, 13, 7, 47, 560, 49, 7, 9, 4, 5),
(52, 12, 9, 5, 2, 3, 0, 7, 5, 9, 5, 90, 551, 65, 20, 11, 12, 5),
(53, 3, 7, 5, 2, 1, 2, 6, 3, 8, 8, 65, 679, 78, 37, 8, 8, 5),
(54, 5, 7, 5, 1, 4, 0, 6, 5, 11, 13, 42, 722, 41, 8, 10, 12, 5),
(55, 4, 6, 5, 1, 3, 1, 5, 4, 5, 12, 51, 675, 59, 6, 15, 7, 5),
(56, 6, 5, 5, 1, 2, 2, 5, 7, 11, 6, 46, 678, 85, 35, 4, 12, 5),
(57, 8, 4, 5, 1, 1, 3, 3, 6, 4, 10, 48, 698, 48, 14, 11, 15, 5),
(58, 11, 4, 5, 1, 1, 3, 2, 6, 11, 8, 91, 759, 62, 6, 4, 10, 5),
(59, 7, 3, 5, 1, 0, 4, 5, 11, 12, 14, 55, 476, 46, 19, 14, 7, 5),
(60, 9, 1, 5, 0, 1, 4, 4, 9, 5, 11, 60, 582, 58, 34, 5, 6, 5),
(61, 1, 14, 6, 4, 2, 0, 8, 3, 7, 11, 75, 743, 41, 10, 14, 12, 6),
(62, 2, 13, 6, 4, 1, 1, 9, 3, 7, 4, 70, 434, 64, 26, 1, 6, 6),
(63, 10, 13, 6, 4, 1, 1, 6, 3, 8, 6, 94, 461, 40, 31, 13, 13, 6),
(64, 3, 10, 6, 3, 1, 2, 7, 3, 14, 9, 46, 701, 89, 39, 14, 15, 6),
(65, 12, 10, 6, 2, 4, 0, 7, 5, 15, 4, 84, 735, 51, 29, 4, 7, 6),
(66, 6, 8, 6, 2, 2, 2, 7, 8, 10, 10, 92, 590, 74, 27, 2, 5, 6),
(67, 4, 7, 6, 1, 4, 1, 5, 4, 8, 10, 83, 453, 75, 34, 15, 11, 6),
(68, 5, 7, 6, 1, 4, 1, 6, 6, 11, 6, 75, 474, 65, 16, 2, 7, 6),
(69, 9, 4, 6, 1, 1, 4, 7, 10, 12, 8, 40, 469, 72, 17, 11, 10, 6),
(70, 8, 4, 6, 1, 1, 4, 4, 9, 6, 12, 92, 577, 78, 23, 7, 4, 6),
(71, 11, 4, 6, 1, 1, 4, 3, 8, 9, 12, 72, 474, 52, 20, 2, 14, 6),
(72, 7, 3, 6, 1, 0, 5, 5, 12, 12, 5, 89, 523, 84, 21, 6, 8, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

DROP TABLE IF EXISTS `equipo`;
CREATE TABLE `equipo` (
  `idequipo` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `idpais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `equipo`:
--   `idpais`
--       `pais` -> `idpais`
--

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`idequipo`, `descripcion`, `idpais`) VALUES
(1, 'Antigua GFC', 1),
(2, 'Cobán Imperial', 1),
(3, 'Comunicaciones', 1),
(4, 'Cotzumalguapa', 1),
(5, 'Deportivo Sanarate', 1),
(6, 'Guastatoya', 1),
(7, 'Iztapa', 1),
(8, 'Malacateco', 1),
(9, 'Mixco', 1),
(10, 'Municipal', 1),
(11, 'Siquinalá', 1),
(12, 'Xelajú', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadio`
--

DROP TABLE IF EXISTS `estadio`;
CREATE TABLE `estadio` (
  `idestadio` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `ubicacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `estadio`:
--

--
-- Volcado de datos para la tabla `estadio`
--

INSERT INTO `estadio` (`idestadio`, `nombre`, `ubicacion`) VALUES
(1, 'Estadio Doroteo Guamuch Flores', 'Ciudad de Guatemala'),
(2, 'Israel Barrios', 'Coatepeque'),
(3, 'Cementos Progreso', 'Ciudad de Guatemala'),
(4, 'Winston Pineda', 'Jutiapa'),
(5, 'Estadio José Ángel Rossi', 'Cobán'),
(6, 'Estadio Ricardo Muñoz Gálvez', 'Santa Lucía Cotzumalguapa'),
(7, 'Estadio Santa Lucía', 'San Marcos'),
(8, 'Estadio del Ejército', 'Ciudad de Guatemala'),
(9, 'Estadio Guillermo Slowing', 'Amatitlán'),
(10, 'Estadio Mario Camposeco', 'Quetzaltenango'),
(11, 'Estadio Armando Barillas', 'Escuintla'),
(12, 'Estadio Carlos Salazar Hijo', 'Suchitepéquez'),
(13, 'Estadio Pensativo', 'Antigua Guatemala'),
(14, 'Estadio Las Flores', 'Jalapa'),
(15, 'Estadio Municipal Mateo Sicay Paz', 'Siquinalá'),
(16, 'Estadio David Cordón Hichos', 'Guastatoya'),
(17, 'Estadio Municipal de Sanarate', 'Sanarate'),
(18, 'Estadio Santo Domingo', 'Mixco'),
(19, 'Estadio Municipal El Morón', 'Iztapa'),
(20, 'Estadio Manuel Felipe Carrera', 'Ciudad de Guatemala'),
(21, 'Estadio Municipal Santa Lucía Cotzumalguapa', 'Santa Lucía Cotzumalguapa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

DROP TABLE IF EXISTS `jugador`;
CREATE TABLE `jugador` (
  `idJugador` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `numJugador` varchar(45) DEFAULT NULL,
  `idequipo` int(11) NOT NULL,
  `idposicion` int(11) NOT NULL,
  `edad` varchar(45) DEFAULT NULL,
  `Altura` varchar(45) DEFAULT NULL,
  `Peso` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `jugador`:
--   `idequipo`
--       `equipo` -> `idequipo`
--   `idposicion`
--       `posicion` -> `idposicion`
--

--
-- Volcado de datos para la tabla `jugador`
--

INSERT INTO `jugador` (`idJugador`, `nombre`, `numJugador`, `idequipo`, `idposicion`, `edad`, `Altura`, `Peso`) VALUES
(1, 'V. Ayala', '33', 1, 4, '30 años', '178cm', '75kg'),
(2, 'C. Alvarez', '', 1, 4, '36 años', '', ''),
(3, 'L. Morán', '31', 1, 4, '21 años', '171cm', ''),
(4, 'O. Castellanos', '17', 1, 1, '19 años', '', ''),
(5, 'O. Esparza', '8', 1, 1, '31 años', '175cm', '75kg'),
(6, 'J. Ardón', '12', 1, 1, '19 años', '', ''),
(7, 'J. Mena', '3', 1, 1, '30 años', '181cm', '78kg'),
(8, 'J. Pinto Samayoa', '6', 1, 1, '26 años', '180cm', '78kg'),
(9, 'E. Fuentes', '20', 1, 1, '28 años', '', ''),
(10, 'C. Jimenez', '26', 1, 1, '24 años', '174cm', '71kg'),
(11, 'J. Osorio', '4', 1, 1, '29 años', '176cm', ''),
(12, 'C. Ojeda', '21', 1, 3, '23 años', '173cm', '75kg'),
(13, 'P. Mingorance', '25', 1, 3, '29 años', '176cm', '75kg'),
(14, 'M. Dominguez-Ramirez', '5', 1, 3, '23 años', '178cm', ''),
(15, 'R. Lezcano', '7', 1, 3, '29 años', '172cm', '75kg'),
(16, 'J. Mora', '21', 1, 3, '28 años', '174cm', '84kg'),
(17, 'J. Ruiz', '14', 1, 3, '24 años', '', ''),
(18, 'B. Ramirez', '', 1, 3, '27 años', '', ''),
(19, 'A. Hurtarte', '', 1, 3, '37 años', '', ''),
(20, 'R. Arce', '18', 1, 3, '26 años', '', ''),
(21, 'P. Aguilar', '10', 1, 3, '24 años', '170cm', ''),
(22, 'Andy Ruiz', '', 1, 3, '23 años', '', ''),
(23, 'J. Arreola', '77', 1, 3, '33 años', '174cm', '68kg'),
(24, 'A. Porras', '9', 1, 2, '28 años', '183cm', ''),
(25, 'A. Barrientos', '23', 1, 2, '25 años', '', ''),
(26, 'F. Villalta', '', 1, 2, '', '', ''),
(27, 'F. Ucros', '', 1, 2, '', '', ''),
(28, 'E. Pacheco', '22', 1, 2, '29 años', '173cm', '73kg'),
(29, 'C. Ramirez', '71', 1, 2, '25 años', '', ''),
(30, 'P. Motta', '12', 2, 4, '36 años', '181cm', ''),
(31, 'B. Linares', '1', 2, 4, '', '', ''),
(32, 'E. Lalín', '22', 2, 4, '', '', ''),
(33, 'J. Sotomayor', '4', 2, 1, '31 años', '179cm', '71kg'),
(34, 'Y. Álvarez', '8', 2, 1, '24 años', '164cm', ''),
(35, 'S. Azurdia', '72', 2, 1, '30 años', '173cm', ''),
(36, 'A. Cifuentes', '3', 2, 1, '29 años', '186cm', '78kg'),
(37, 'E. Morales', '23', 2, 1, '34 años', '179cm', '77kg'),
(38, 'D. Guay', '80', 2, 3, '21 años', '172cm', '67kg'),
(39, 'V. Guay', '20', 2, 3, '24 años', '172cm', '78kg'),
(40, 'N. Oliva', '14', 2, 3, '30 años', '', ''),
(41, 'J. Klug Ramírez', '18', 2, 3, '', '', ''),
(42, 'B. Leal', '21', 2, 3, '29 años', '175cm', '72kg'),
(43, 'E. Klug', '5', 2, 3, '29 años', '169cm', '71kg'),
(44, 'A. Cabrera', '17', 2, 3, '23 años', '160cm', '54kg'),
(45, 'Janderson', '28', 2, 3, '30 años', '175cm', '75kg'),
(46, 'O. Flores', '', 2, 3, '31 años', '173cm', '67kg'),
(47, 'Y. Ramos', '27', 2, 3, '24 años', '168cm', '65kg'),
(48, 'Y. Morán', '48', 2, 3, '21 años', '170cm', '68kg'),
(49, 'J. Mitchell', '11', 2, 2, '29 años', '192cm', '83kg'),
(50, 'E. Soto', '', 2, 2, '29 años', '165cm', '69kg'),
(51, 'L. Cazal', '29', 2, 2, '33 años', '188cm', '84kg'),
(52, 'G. Tinoco', '9', 2, 2, '30 años', '175cm', ''),
(53, 'F. Pérez', '27', 3, 4, '24 años', '184cm', '73kg'),
(54, 'K. Moscoso', '23', 3, 4, '', '', ''),
(55, 'J. Calderón', '1', 3, 4, '34 años', '190cm', '84kg'),
(56, 'C. Estrada', '12', 3, 1, '22 años', '179cm', ''),
(57, 'N. Samayoa', '3', 3, 1, '24 años', '188cm', '79kg'),
(58, 'M. Umaña', '4', 3, 1, '37 años', '177cm', '75kg'),
(59, 'Rafael Morales', '14', 3, 1, '31 años', '177cm', '73kg'),
(60, 'G. Gordillo', '20', 3, 1, '25 años', '187cm', ''),
(61, 'C. Castrillo', '13', 3, 1, '34 años', '178cm', '73kg'),
(62, 'C. Mejia', '', 3, 1, '27 años', '165cm', '59kg'),
(63, 'C. A. Hernández', '5', 3, 3, '24 años', '', ''),
(64, 'O. Mejía', '', 3, 3, '20 años', '', ''),
(65, 'S. Cincotta', '18', 3, 3, '28 años', '177cm', '70kg'),
(66, 'K. Lemus', '16', 3, 3, '26 años', '', ''),
(67, 'R. Saravia', '8', 3, 3, '26 años', '180cm', '77kg'),
(68, 'A. Galindo', '17', 3, 3, '27 años', '182cm', '78kg'),
(69, 'J. Vargas', '15', 3, 3, '26 años', '170cm', ''),
(70, 'D. Chinchilla', '32', 3, 3, '19 años', '', ''),
(71, 'L. García', '26', 3, 3, '', '', ''),
(72, 'R. Rodríguez', '24', 3, 3, '21 años', '164cm', '54kg'),
(73, 'F. Thompson', '28', 3, 3, '37 años', '171cm', '73kg'),
(74, 'J. Contreras', '', 3, 3, '33 años', '176cm', '69kg'),
(75, 'M. Lombardi', '7', 3, 3, '32 años', '175cm', '72kg'),
(76, 'N. García', '34', 3, 3, '', '', ''),
(77, 'S. Robles', '31', 3, 3, '23 años', '', ''),
(78, 'R. Betancourth', '19', 3, 2, '27 años', '180cm', '72kg'),
(79, 'E. Chinchilla', '9', 3, 2, '32 años', '180cm', ''),
(80, 'A. Rodríguez', '', 3, 2, '34 años', '180cm', '82kg'),
(81, 'K. Grijalva', '2', 3, 2, '24 años', '165cm', ''),
(82, 'A. Herrera', '11', 3, 2, '34 años', '175cm', '68kg'),
(83, 'M. Bonilla', '', 4, 4, '26 años', '175cm', '66kg'),
(84, 'M. Mendoza', '1', 4, 4, '', '', ''),
(85, 'M. Sosa', '52', 4, 4, '32 años', '181cm', '73kg'),
(86, 'Z. Martínez', '52', 4, 4, '', '', ''),
(87, 'A. Rivera', '6', 4, 1, '35 años', '176cm', '68kg'),
(88, 'A. Sapon', '15', 4, 1, '23 años', '', ''),
(89, 'C. Barros', '5', 4, 3, '28 años', '', ''),
(90, 'K. Guzmán', '8', 4, 3, '33 años', '182cm', '62kg'),
(91, 'C. Solares', '7', 4, 3, '26 años', '', ''),
(92, 'M. Icute', '21', 4, 3, '31 años', '168cm', ''),
(93, 'O. Rodas', '', 4, 3, '', '', ''),
(94, 'M. Farfán', '11', 4, 3, '31 años', '169cm', ''),
(95, 'R. Barrera', '19', 4, 3, '', '', ''),
(96, 'H. Contreras Milián', '17', 4, 3, '35 años', '', ''),
(97, 'J. Velásquez', '10', 4, 3, '26 años', '', ''),
(98, 'W. Díaz', '14', 4, 3, '28 años', '', ''),
(99, 'D. Sánchez Corrales', '15', 4, 3, '21 años', '169cm', ''),
(100, 'J. López', '12', 4, 3, '31 años', '176cm', '81kg'),
(101, 'J. Del Aguila', '', 4, 3, '28 años', '178cm', '72kg'),
(102, 'Rafinha', '80', 4, 3, '30 años', '172cm', '70kg'),
(103, 'Romario', '99', 4, 2, '25 años', '', ''),
(104, 'O. Martinez', '16', 4, 2, '33 años', '181cm', ''),
(105, 'C. Rodriguez', '22', 4, 2, '30 años', '184cm', '87kg'),
(106, 'L. Nieves', '9', 4, 2, '34 años', '179cm', '77kg'),
(107, 'M. Estrada', '18', 4, 2, '28 años', '168cm', '72kg'),
(108, 'C. Villavicencio', '3', 4, 2, '', '', ''),
(109, 'A. Trigueros', '', 4, 2, '31 años', '', ''),
(110, 'C. Herrera', '30', 5, 4, '22 años', '183cm', '59kg'),
(111, 'D. Silva', '1', 5, 4, '27 años', '179cm', ''),
(112, 'D. Monterroso', '27', 5, 4, '', '', ''),
(113, 'A. Ortiz', '4', 5, 1, '', '', ''),
(114, 'E. Chacon', '', 5, 1, '35 años', '177cm', ''),
(115, 'Nixsón Flores', '22', 5, 1, '26 años', '', ''),
(116, 'J. Salazar', '5', 5, 1, '28 años', '', ''),
(117, 'N. Martinez Vargas', '9', 5, 1, '28 años', '175cm', '76kg'),
(118, 'C. Calderon', '11', 5, 1, '23 años', '', ''),
(119, 'T. Castillo', '16', 5, 1, '28 años', '177cm', '75kg'),
(120, 'J. Saldivar', '', 5, 1, '34 años', '', ''),
(121, 'E. Vásquez', '3', 5, 1, '27 años', '180cm', '77kg'),
(122, 'D. Palencia', '23', 5, 3, '18 años', '', ''),
(123, 'B. Aldana', '8', 5, 3, '23 años', '163cm', '67kg'),
(124, 'N. Jucup', '7', 5, 3, '30 años', '171cm', ''),
(125, 'J. Posas', '', 5, 3, '32 años', '175cm', '72kg'),
(126, 'W. Garcia', '15', 5, 3, '27 años', '', ''),
(127, 'P. Altan', '88', 5, 3, '22 años', '', ''),
(128, 'A. Rodas', '12', 5, 3, '27 años', '178cm', '68kg'),
(129, 'J. Silva', '6', 5, 3, '31 años', '171cm', '72kg'),
(130, 'K. Aguilar', '14', 5, 3, '', '', ''),
(131, 'K. Ávila', '17', 5, 3, '25 años', '', ''),
(132, 'W. Zapata', '10', 5, 2, '31 años', '186cm', '83kg'),
(133, 'O. Martinez', '', 5, 2, '33 años', '181cm', ''),
(134, 'K. Elías', '13', 5, 2, '25 años', '175cm', '74kg'),
(135, 'J. Paredes', '', 6, 4, '34 años', '176cm', '72kg'),
(136, 'E. Rodríguez', '12', 6, 4, '28 años', '177cm', '72kg'),
(137, 'Brayan Hernández', '99', 6, 4, '19 años', '', ''),
(138, 'A. De Lemos', '1', 6, 4, '36 años', '184cm', '80kg'),
(139, 'S. Crisanto', '3', 6, 1, '', '', ''),
(140, 'O. Domínguez', '2', 6, 1, '31 años', '176cm', '76kg'),
(141, 'J. Corena', '28', 6, 1, '26 años', '177cm', '75kg'),
(142, 'D. Morales', '16', 6, 1, '32 años', '180cm', ''),
(143, 'Omar', '2', 6, 1, '31 años', '', ''),
(144, 'B. Lemus', '32', 6, 1, '25 años', '170cm', '65kg'),
(145, 'W. Pineda', '22', 6, 1, '25 años', '178cm', '73kg'),
(146, 'J. Gatgens', '8', 6, 3, '31 años', '178cm', '65kg'),
(147, 'K. Herrarte', '24', 6, 3, '27 años', '169cm', '62kg'),
(148, 'W. Sagastume', '17', 6, 3, '', '', ''),
(149, 'J. Aparicio', '25', 6, 3, '26 años', '176cm', '67kg'),
(150, 'J. Márquez', '5', 6, 3, '31 años', '179cm', ''),
(151, 'D. Alvarez', '', 6, 3, '24 años', '', ''),
(152, 'A. Navarro', '11', 6, 3, '32 años', '174cm', '72kg'),
(153, 'D. Sánchez', '17', 6, 3, '', '', ''),
(154, 'Cristian Reyes', '20', 6, 2, '27 años', '', ''),
(155, 'D. Guzmán', '10', 6, 2, '27 años', '178cm', '72kg'),
(156, 'N. Miranda', '19', 6, 2, '28 años', '175cm', '68kg'),
(157, 'C. Lima', '6', 6, 2, '26 años', '', ''),
(158, 'L. Landín', '27', 6, 2, '34 años', '184cm', '87kg'),
(159, 'F. Orellana', '18', 6, 2, '28 años', '168cm', '66kg'),
(160, 'L. Martinez', '7', 6, 2, '27 años', '172cm', ''),
(161, 'E. Hernández', '23', 7, 4, '', '', ''),
(162, 'L. Sánchez', '22', 7, 4, '29 años', '183cm', '78kg'),
(163, 'W. Lalin', '3', 7, 1, '34 años', '180cm', '80kg'),
(164, 'A.torres', '20', 7, 1, '', '', ''),
(165, 'J. Matul', '', 7, 1, '25 años', '', ''),
(166, 'E. Díaz', '12', 7, 1, '29 años', '182cm', '77kg'),
(167, 'Sebastián Borba', '', 7, 1, '29 años', '', ''),
(168, 'E. Cabrera', '18', 7, 1, '28 años', '176cm', '74kg'),
(169, 'R. García', '2', 7, 3, '24 años', '', ''),
(170, 'C. Guerra', '16', 7, 3, '24 años', '', ''),
(171, 'M. Ramos', '80', 7, 3, '', '', ''),
(172, 'N. Grajeda', '11', 7, 3, '31 años', '176cm', '74kg'),
(173, 'R. Murga', '', 7, 3, '39 años', '', ''),
(174, 'A. Quiñonez', '17', 7, 3, '23 años', '', ''),
(175, 'J. Perez', '', 7, 3, '31 años', '', ''),
(176, 'V. Gudiel', '15', 7, 3, '', '', ''),
(177, 'K. Arriola', '', 7, 3, '28 años', '175cm', '67kg'),
(178, 'K. Santamaría', '10', 7, 3, '28 años', '173cm', '73kg'),
(179, 'J. Lopez', '77', 7, 3, '31 años', '175cm', '63kg'),
(180, 'J. Ramos', '7', 7, 3, '', '', ''),
(181, 'B. Barrios', '43', 7, 3, '25 años', '168cm', '54kg'),
(182, 'K. García', '6', 7, 3, '28 años', '180cm', '68kg'),
(183, 'I. Alemán', '88', 7, 3, '', '', ''),
(184, 'R. Rocca', '9', 7, 2, '30 años', '', ''),
(185, 'M. Asencio', '13', 7, 2, '32 años', '174cm', '67kg'),
(186, 'L. Gonzáles', '', 7, 2, '31 años', '', ''),
(187, 'Sergio Blancas', '19', 7, 2, '31 años', '180cm', '80kg'),
(188, 'L. Gonzáles', '', 7, 2, '', '', ''),
(189, 'D. Palma', '', 7, 2, '23 años', '', ''),
(190, 'D. Cambronero', '1', 8, 4, '33 años', '180cm', '74kg'),
(191, 'J. Ochoa', '30', 8, 4, '24 años', '178cm', '78kg'),
(192, 'C. Avedissian', '', 8, 4, '26 años', '', ''),
(193, 'J. Dávila', '25', 8, 4, '26 años', '185cm', '68kg'),
(194, 'J. Castañeda', '4', 8, 1, '24 años', '181cm', '70kg'),
(195, 'S. Betancourt', '19', 8, 1, '27 años', '179cm', '66kg'),
(196, 'O. Osorio', '24', 8, 1, '29 años', '184cm', '77kg'),
(197, 'F. Lopez', '99', 8, 1, '27 años', '174cm', '61kg'),
(198, 'R. Maltes', '22', 8, 1, '22 años', '', ''),
(199, 'C. De León', '', 8, 3, '20 años', '', ''),
(200, 'D. Ruiz', '9', 8, 3, '27 años', '', ''),
(201, 'J. Sánchez', '14', 8, 3, '28 años', '178cm', '80kg'),
(202, 'M. De León', '7', 8, 3, '28 años', '166cm', '70kg'),
(203, 'C. Alvarado', '11', 8, 3, '', '', ''),
(204, 'K. Ruiz', '5', 8, 3, '24 años', '179cm', '73kg'),
(205, 'J. Cardona', '17', 8, 3, '26 años', '177cm', '72kg'),
(206, 'E. Moscoso', '23', 8, 3, '', '', ''),
(207, 'D. Reyes', '16', 8, 3, '22 años', '', ''),
(208, 'B. Menéndez', '3', 8, 3, '24 años', '176cm', '63kg'),
(209, 'M. Sequen', '31', 8, 3, '26 años', '', ''),
(210, 'K. Ramírez', '23', 8, 3, '17 años', '', ''),
(211, 'S. de León', '', 8, 3, '39 años', '179cm', '73kg'),
(212, 'Jonathan García', '', 8, 3, '25 años', '180cm', ''),
(213, 'C. Innella', '32', 8, 3, '28 años', '185cm', ''),
(214, 'E. Herrera', '27', 8, 2, '27 años', '185cm', '78kg'),
(215, 'A. Francis', '8', 8, 2, '28 años', '175cm', '73kg'),
(216, 'J. Longo', '77', 8, 2, '25 años', '176cm', ''),
(217, 'W. Perez', '81', 8, 2, '30 años', '181cm', '68kg'),
(218, 'M. Nicolau', '1', 9, 4, '', '', ''),
(219, 'R. Alvarez', '1', 9, 4, '39 años', '', ''),
(220, 'L. Tatuaca', '32', 9, 4, '29 años', '178cm', '73kg'),
(221, 'J. Flores', '14', 9, 1, '34 años', '', ''),
(222, 'D. Franco', '77', 9, 1, '23 años', '155cm', '65kg'),
(223, 'H. López', '34', 9, 1, '35 años', '180cm', ''),
(224, 'J. Giron', '31', 9, 1, '36 años', '179cm', '74kg'),
(225, 'G. Flores', '3', 9, 1, '24 años', '162cm', '56kg'),
(226, 'E. Aragon', '16', 9, 1, '34 años', '185cm', ''),
(227, 'J. Milla', '8', 9, 3, '22 años', '', ''),
(228, 'J. Márquez', '12', 9, 3, '34 años', '171cm', '73kg'),
(229, 'M. Pappa', '10', 9, 3, '31 años', '179cm', '68kg'),
(230, 'K. Marroquín', '6', 9, 3, '30 años', '160cm', '61kg'),
(231, 'Brolin Valdizón', '92', 9, 3, '20 años', '', ''),
(232, 'P. Samayoa', '17', 9, 3, '34 años', '171cm', '71kg'),
(233, 'M. Meneses', '20', 9, 3, '', '', ''),
(234, 'D. Marroquín', '23', 9, 3, '24 años', '', ''),
(235, 'L. Cos', '27', 9, 3, '22 años', '', ''),
(236, 'B. Ordóñez', '9', 9, 3, '28 años', '166cm', '66kg'),
(237, 'M. Aragon', '21', 9, 3, '', '', ''),
(238, 'D. Bardales', '23', 9, 3, '', '', ''),
(239, 'B. Santizo', '15', 9, 2, '24 años', '170cm', '68kg'),
(240, 'L. Rodas', '10', 9, 2, '28 años', '180cm', '75kg'),
(241, 'J. Valenzuela', '89', 9, 2, '30 años', '168cm', '66kg'),
(242, 'M. López', '18', 9, 2, '32 años', '194cm', '88kg'),
(243, 'A. Ubeda', '72', 10, 4, '29 años', '183cm', '70kg'),
(244, 'N. Hagen', '1', 10, 4, '23 años', '187cm', '81kg'),
(245, 'K. Navarro', '81', 10, 4, '17 años', '', ''),
(246, 'C. Gallardo', '3', 10, 1, '35 años', '188cm', '77kg'),
(247, 'J. Payeras', '28', 10, 1, '25 años', '188cm', ''),
(248, 'C. Alvarado', '8', 10, 1, '20 años', '171cm', '68kg'),
(249, 'A. Tuna', '', 10, 1, '25 años', '170cm', '65kg'),
(250, 'H. Moreira', '5', 10, 1, '31 años', '183cm', ''),
(251, 'Brandon Andrés', '30', 10, 3, '25 años', '173cm', '68kg'),
(252, 'J. Rosales', '6', 10, 3, '26 años', '', ''),
(253, 'N. Cifuentes', '17', 10, 3, '', '', ''),
(254, 'F. De Leon', '7', 10, 3, '24 años', '171cm', '60kg'),
(255, 'E. Marroquín', '23', 10, 3, '', '', ''),
(256, 'J. Fajardo', '12', 10, 3, '22 años', '', ''),
(257, 'R. Barrientos', '26', 10, 3, '20 años', '175cm', '60kg'),
(258, 'L. De León', '4', 10, 3, '23 años', '', ''),
(259, 'F. Mota', '', 10, 3, '19 años', '', ''),
(260, 'O. Moreira', '27', 10, 3, '27 años', '187cm', '86kg'),
(261, 'J. Alas', '14', 10, 3, '30 años', '172cm', '68kg'),
(262, 'M. López', '2', 10, 3, '29 años', '179cm', ''),
(263, 'H. Morales', '13', 10, 3, '21 años', '', ''),
(264, 'C. Monterroso', '97', 10, 3, '', '', ''),
(265, 'Brandon Rivas', '', 10, 2, '25 años', '', ''),
(266, 'D. Guerra', '20', 10, 2, '31 años', '178cm', ''),
(267, 'O. Arce', '10', 10, 2, '29 años', '189cm', '77kg'),
(268, 'H. Quezada', '16', 10, 2, '21 años', '', ''),
(269, 'J. Martínez', '9', 10, 2, '21 años', '', ''),
(270, 'A. Diaz', '19', 10, 2, '30 años', '165cm', '61kg'),
(271, 'R. Méndez', '1', 11, 4, '29 años', '174cm', '81kg'),
(272, 'J. Reyes', '', 11, 4, '34 años', '184cm', '77kg'),
(273, 'I. Pacheco', '27', 11, 4, '27 años', '180cm', '66kg'),
(274, 'B. Morales', '2', 11, 1, '24 años', '172cm', '73kg'),
(275, 'J. Morales', '14', 11, 1, '22 años', '', ''),
(276, 'D. Hernández', '22', 11, 1, '22 años', '', ''),
(277, 'W. Techera', '4', 11, 1, '33 años', '180cm', '77kg'),
(278, 'F. Ruano', '77', 11, 3, '29 años', '168cm', '63kg'),
(279, 'L. Gálvez', '24', 11, 3, '22 años', '162cm', '62kg'),
(280, 'L. Estrada', '20', 11, 3, '', '', ''),
(281, 'L. Rosas', '21', 11, 3, '20 años', '', ''),
(282, 'M. Avila', '19', 11, 3, '33 años', '187cm', '73kg'),
(283, 'L. Juárez', '28', 11, 3, '', '', ''),
(284, 'D. Reyes', '13', 11, 3, '', '', ''),
(285, 'R. Escobar', '13', 11, 3, '', '', ''),
(286, 'J. Priego', '8', 11, 3, '31 años', '168cm', '64kg'),
(287, 'J. Del Aguila', '17', 11, 3, '28 años', '178cm', '72kg'),
(288, 'O. Rodas', '9', 11, 3, '', '', ''),
(289, 'E. Ardón', '7', 11, 3, '', '', ''),
(290, 'Sun You-Kin', '18', 11, 3, '24 años', '175cm', '75kg'),
(291, 'Álvaro', '10', 11, 2, '32 años', '', ''),
(292, 'L. Pineda', '', 11, 2, '20 años', '', ''),
(293, 'J. Ortega', '11', 11, 2, '20 años', '', ''),
(294, 'M. Guardia', '15', 11, 2, '28 años', '180cm', '70kg'),
(295, 'Elías Monterroso', '', 12, 4, '32 años', '', ''),
(296, 'Á. García', '1', 12, 4, '35 años', '180cm', '76kg'),
(297, 'José García', '21', 12, 4, '26 años', '190cm', '85kg'),
(298, 'J. Gónzalez', '15', 12, 1, '', '178cm', '65kg'),
(299, 'M. Moreno', '3', 12, 1, '27 años', '178cm', ''),
(300, 'K. Agustín', '', 12, 1, '24 años', '', ''),
(301, 'M. Leal', '14', 12, 1, '36 años', '183cm', ''),
(302, 'D. Ferreira', '20', 12, 1, '32 años', '186cm', '85kg'),
(303, 'R. González', '4', 12, 1, '34 años', '176cm', '73kg'),
(304, 'G. Arias', '16', 12, 1, '33 años', '174cm', '68kg'),
(305, 'E. Santeliz', '11', 12, 3, '32 años', '170cm', '66kg'),
(306, 'W. Godoy', '12', 12, 3, '32 años', '175cm', '76kg'),
(307, 'E. García', '77', 12, 3, '21 años', '', ''),
(308, 'A. Matta', '91', 12, 3, '29 años', '169cm', ''),
(309, 'J. Yax', '25', 12, 3, '31 años', '175cm', ''),
(310, 'K. Norales', '19', 12, 3, '27 años', '161cm', '61kg'),
(311, 'E. Macal', '7', 12, 3, '28 años', '168cm', '60kg'),
(312, 'C. Albizures', '10', 12, 3, '24 años', '163cm', '58kg'),
(313, 'M. Castellanos', '9', 12, 2, '37 años', '175cm', '76kg'),
(314, 'G. Vivanco', '84', 12, 2, '30 años', '', ''),
(315, 'C. Félix', '32', 12, 2, '34 años', '180cm', ''),
(316, 'J. Ortiz', '90', 12, 2, '29 años', '186cm', '73kg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador_has_calificacion`
--

DROP TABLE IF EXISTS `jugador_has_calificacion`;
CREATE TABLE `jugador_has_calificacion` (
  `iddetalle` int(11) NOT NULL,
  `idJugador` int(11) NOT NULL,
  `idpartido` int(11) NOT NULL,
  `idcalificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `jugador_has_calificacion`:
--   `idcalificacion`
--       `calificacion` -> `idcalificacion`
--   `idJugador`
--       `jugador` -> `idJugador`
--

--
-- Volcado de datos para la tabla `jugador_has_calificacion`
--

INSERT INTO `jugador_has_calificacion` (`iddetalle`, `idJugador`, `idpartido`, `idcalificacion`) VALUES
(1, 96, 1, 2),
(2, 96, 1, 3),
(3, 103, 1, 1),
(4, 121, 1, 2),
(5, 115, 1, 2),
(6, 129, 1, 2),
(7, 287, 2, 2),
(8, 287, 2, 3),
(9, 291, 2, 1),
(10, 291, 2, 2),
(11, 278, 2, 2),
(12, 288, 2, 2),
(13, 224, 2, 2),
(14, 234, 2, 2),
(15, 242, 2, 2),
(16, 59, 3, 2),
(17, 60, 3, 2),
(18, 70, 3, 2),
(19, 33, 3, 2),
(20, 37, 3, 2),
(21, 44, 3, 2),
(22, 310, 4, 2),
(23, 306, 4, 2),
(24, 309, 4, 2),
(25, 224, 4, 2),
(26, 248, 4, 2),
(27, 21, 5, 2),
(28, 24, 5, 2),
(29, 20, 5, 1),
(30, 15, 5, 1),
(31, 15, 5, 2),
(32, 196, 5, 2),
(33, 201, 5, 2),
(34, 207, 5, 2),
(35, 142, 6, 1),
(36, 141, 6, 2),
(37, 155, 6, 1),
(38, 160, 6, 2),
(39, 166, 6, 2),
(40, 172, 6, 2),
(41, 172, 6, 3),
(42, 188, 6, 2),
(43, 124, 7, 2),
(44, 132, 7, 2),
(45, 277, 7, 2),
(46, 275, 7, 2),
(47, 276, 7, 2),
(48, 278, 7, 2),
(49, 33, 8, 1),
(50, 34, 8, 2),
(51, 43, 8, 2),
(52, 43, 8, 3),
(53, 39, 8, 2),
(54, 51, 8, 1),
(55, 87, 8, 2),
(56, 92, 8, 2),
(57, 98, 8, 2),
(58, 108, 8, 2),
(59, 102, 8, 1),
(60, 106, 8, 2),
(61, 254, 9, 2),
(62, 258, 9, 2),
(63, 266, 9, 1),
(64, 266, 9, 2),
(65, 61, 9, 2),
(66, 61, 9, 3),
(67, 73, 9, 2),
(68, 67, 9, 2),
(69, 82, 9, 2),
(70, 77, 9, 2),
(71, 68, 9, 2),
(72, 310, 10, 2),
(73, 304, 10, 2),
(74, 204, 10, 2),
(75, 305, 10, 2),
(76, 205, 10, 2),
(77, 214, 10, 2),
(78, 196, 10, 2),
(79, 205, 10, 2),
(80, 296, 10, 2),
(81, 302, 10, 2),
(82, 306, 10, 2),
(83, 160, 11, 1),
(84, 226, 11, 1),
(85, 224, 11, 2),
(86, 142, 11, 2),
(87, 223, 11, 2),
(88, 154, 11, 2),
(89, 15, 12, 1),
(90, 25, 12, 2),
(91, 178, 12, 2),
(92, 17, 12, 2),
(93, 14, 12, 2),
(94, 166, 12, 2),
(95, 6, 12, 2),
(96, 1, 12, 2),
(97, 179, 12, 2),
(98, 4, 12, 2),
(99, 49, 13, 1),
(100, 33, 13, 1),
(101, 277, 13, 2),
(102, 37, 13, 2),
(103, 276, 13, 2),
(104, 275, 13, 2),
(105, 254, 14, 1),
(106, 250, 14, 2),
(107, 216, 14, 2),
(108, 196, 14, 2),
(109, 117, 15, 1),
(110, 124, 15, 1),
(111, 158, 15, 1),
(112, 132, 15, 1),
(113, 158, 15, 1),
(114, 117, 15, 2),
(115, 141, 15, 2),
(116, 160, 15, 2),
(117, 129, 15, 2),
(118, 113, 15, 2),
(119, 149, 15, 2),
(120, 140, 15, 3),
(121, 150, 15, 2),
(122, 131, 15, 2),
(123, 315, 16, 1),
(124, 305, 16, 1),
(125, 187, 16, 1),
(126, 315, 16, 1),
(127, 186, 16, 1),
(128, 168, 16, 2),
(129, 166, 16, 2),
(130, 302, 16, 2),
(131, 182, 16, 2),
(132, 172, 16, 2),
(133, 308, 16, 2),
(134, 302, 16, 2),
(135, 58, 17, 1),
(136, 76, 17, 1),
(137, 78, 17, 2),
(138, 65, 17, 2),
(139, 58, 17, 2),
(140, 102, 17, 2),
(141, 75, 17, 2),
(142, 100, 17, 2),
(143, 58, 18, 1),
(144, 76, 18, 1),
(145, 78, 18, 2),
(146, 65, 18, 2),
(147, 58, 18, 2),
(148, 102, 18, 2),
(149, 75, 18, 2),
(150, 100, 18, 2),
(151, 242, 19, 1),
(152, 24, 19, 1),
(153, 228, 19, 1),
(154, 28, 19, 1),
(155, 219, 19, 1),
(156, 242, 19, 2),
(157, 230, 19, 2),
(158, 12, 19, 2),
(159, 238, 19, 2),
(160, 236, 19, 2),
(161, 221, 19, 2),
(162, 24, 19, 2),
(163, 15, 19, 2),
(164, 187, 20, 1),
(165, 267, 20, 1),
(166, 184, 20, 1),
(167, 269, 20, 1),
(168, 184, 20, 1),
(169, 258, 20, 2),
(170, 166, 20, 2),
(171, 266, 20, 2),
(172, 271, 20, 2),
(173, 162, 20, 2),
(174, 82, 21, 1),
(175, 68, 21, 1),
(176, 77, 21, 1),
(177, 198, 21, 2),
(178, 214, 21, 2),
(179, 213, 21, 2),
(180, 51, 22, 1),
(181, 52, 22, 1),
(182, 49, 22, 1),
(183, 157, 22, 2),
(184, 141, 22, 2),
(185, 139, 22, 2),
(186, 40, 22, 2),
(187, 37, 22, 2),
(188, 13, 23, 2),
(189, 8, 23, 2),
(190, 116, 23, 2),
(191, 23, 23, 2),
(192, 12, 23, 2),
(193, 124, 23, 2),
(194, 134, 23, 2),
(195, 28, 23, 2),
(196, 97, 24, 1),
(197, 103, 24, 1),
(198, 242, 24, 1),
(199, 315, 24, 1),
(200, 305, 24, 1),
(201, 302, 24, 2),
(202, 227, 24, 2),
(203, 305, 24, 2),
(204, 78, 25, 1),
(205, 76, 25, 1),
(206, 66, 25, 2),
(207, 277, 25, 2),
(208, 278, 25, 3),
(209, 57, 25, 2),
(210, 276, 25, 2),
(211, 266, 26, 1),
(212, 242, 26, 2),
(213, 223, 26, 2),
(214, 261, 26, 2),
(215, 132, 27, 1),
(216, 312, 27, 1),
(217, 129, 27, 1),
(218, 121, 27, 2),
(219, 301, 27, 2),
(220, 132, 27, 2),
(221, 304, 27, 2),
(222, 128, 27, 3),
(223, 15, 28, 1),
(224, 49, 28, 1),
(225, 6, 28, 1),
(226, 12, 28, 2),
(227, 13, 28, 2),
(228, 45, 28, 2),
(229, 9, 28, 2),
(230, 217, 29, 1),
(231, 197, 29, 1),
(232, 203, 29, 1),
(233, 202, 29, 2),
(234, 172, 29, 2),
(235, 162, 29, 2),
(236, 194, 29, 2),
(237, 102, 30, 2),
(238, 142, 30, 2),
(239, 152, 30, 2),
(240, 290, 31, 1),
(241, 155, 31, 1),
(242, 158, 31, 1),
(243, 283, 31, 2),
(244, 149, 31, 2),
(245, 279, 31, 2),
(246, 152, 31, 2),
(247, 158, 31, 2),
(248, 78, 33, 1),
(249, 67, 33, 2),
(250, 163, 33, 2),
(251, 168, 33, 2),
(252, 172, 33, 2),
(253, 68, 33, 2),
(254, 267, 34, 1),
(255, 260, 34, 2),
(256, 252, 34, 2),
(257, 117, 34, 2),
(258, 87, 35, 2),
(259, 102, 35, 2),
(260, 15, 35, 2),
(261, 9, 35, 2),
(262, 29, 35, 2),
(263, 98, 35, 2),
(264, 242, 36, 1),
(265, 235, 36, 1),
(266, 229, 36, 1),
(267, 217, 36, 1),
(268, 241, 36, 2),
(269, 226, 36, 2),
(270, 209, 36, 2),
(271, 227, 36, 2),
(272, 214, 36, 2),
(273, 196, 36, 2),
(274, 194, 36, 2),
(275, 201, 36, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `idpais` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `pais`:
--

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idpais`, `descripcion`) VALUES
(1, 'Guatemala');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

DROP TABLE IF EXISTS `partido`;
CREATE TABLE `partido` (
  `idpartido` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `equpo1` int(11) NOT NULL,
  `equpo2` int(11) NOT NULL,
  `idestadio` int(11) NOT NULL,
  `jornada` int(11) DEFAULT NULL,
  `resultado1` int(11) DEFAULT NULL,
  `resultado2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `partido`:
--   `equpo1`
--       `equipo` -> `idequipo`
--   `equpo2`
--       `equipo` -> `idequipo`
--   `idestadio`
--       `estadio` -> `idestadio`
--

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`idpartido`, `nombre`, `fecha`, `equpo1`, `equpo2`, `idestadio`, `jornada`, `resultado1`, `resultado2`) VALUES
(1, 'Cotzumalguapa vs Sanarate', '2019-07-28 00:00:00', 4, 5, 21, 1, 0, 0),
(2, 'Siquinalá vs Mixco', '2019-07-27 00:00:00', 11, 9, 15, 1, 0, 0),
(3, 'Comunicaciones vs Cobán Imperial', '2019-07-27 00:00:00', 3, 2, 1, 1, 0, 0),
(4, 'Xelajú MC vs Municipal', '2019-07-27 00:00:00', 12, 10, 10, 1, 0, 0),
(5, 'Antigua GFC vs Malacateco', '2019-07-28 00:00:00', 1, 8, 13, 1, 0, 0),
(6, 'Guastatoya vs Iztapa', '2019-07-28 00:00:00', 6, 7, 16, 1, 0, 0),
(7, 'Deportivo Sanarate vs Siquinalá', '2019-08-03 00:00:00', 5, 11, 17, 2, 0, 0),
(8, 'Cobán Imperial vs Corzumalguapa', '2019-08-03 00:00:00', 2, 4, 5, 2, 0, 0),
(9, 'Municipal vs Comunicaciones', '2019-08-03 00:00:00', 10, 3, 1, 2, 0, 0),
(10, 'Malacateco vs Xelajú', '2019-08-03 00:00:00', 8, 12, 7, 2, 0, 0),
(11, 'Mixco vs Guastatoya', '2019-08-03 00:00:00', 9, 6, 18, 2, 0, 0),
(12, 'Iztapa vs Antigua GFC', '2019-08-03 00:00:00', 7, 1, 19, 2, 0, 0),
(13, 'Siquinalá vs Coban Imperial', '2019-08-09 00:00:00', 11, 2, 15, 3, 0, 0),
(14, 'Municipal vs Malacateco', '2019-08-09 00:00:00', 10, 8, 20, 3, 0, 0),
(15, 'Guastatoya vs Deportivo Sanarate', '2019-08-09 00:00:00', 6, 5, 16, 3, 0, 0),
(16, 'Xelajú vs Iztapa', '2019-08-14 00:00:00', 12, 7, 10, 3, 0, 0),
(17, 'Cotzumalguapa vs Comunicaciones', '2019-08-14 00:00:00', 4, 3, 21, 3, 0, 0),
(18, 'Antigua GFC vs Mixco', '2019-08-09 00:00:00', 1, 9, 13, 3, 0, 0),
(19, 'Iztapa vs Municipal', '2019-08-17 00:00:00', 7, 10, 19, 4, 0, 0),
(20, 'Comunicaciones vs Malacateco', '2019-08-17 00:00:00', 3, 8, 1, 4, 0, 0),
(21, 'Cobán Imperial vs Guastatoya', '2019-08-17 00:00:00', 2, 6, 5, 4, 0, 0),
(22, 'Deportivo Sanarate vs Antigua GFC', '2019-08-18 00:00:00', 5, 1, 17, 4, 0, 0),
(23, 'Cotzumalguapa vs Siquinalá', '2019-08-18 00:00:00', 4, 11, 21, 4, 0, 0),
(24, 'Mixco vs Xelajú', '2019-08-18 00:00:00', 9, 12, 18, 4, 0, 0),
(25, 'Siquinalá vs Comunicaciones', '2019-08-24 00:00:00', 11, 3, 15, 5, 0, 0),
(26, 'Municipal vs Mixco', '2019-08-24 00:00:00', 10, 9, 20, 5, 0, 0),
(27, 'Xelajú vs Deportivo Sanarate', '2019-08-24 00:00:00', 12, 5, 10, 5, 0, 0),
(28, 'Antigua GFC vs Cobán Imperial', '2019-08-25 00:00:00', 1, 2, 13, 5, 0, 0),
(29, 'Malacateco vs Iztapa', '2019-08-25 00:00:00', 8, 7, 7, 5, 0, 0),
(30, 'Guastatoya vs Cotzumalguapa', '2019-08-25 00:00:00', 6, 4, 16, 5, 0, 0),
(31, 'Siquinalá vs Guastatoya', '2019-08-31 00:00:00', 11, 6, 15, 6, 0, 0),
(32, 'Cobán Imperial vs Xelajú', '2019-08-31 00:00:00', 2, 12, 5, 6, 0, 0),
(33, 'Comuncaciones vs Iztapa', '2019-08-31 00:00:00', 3, 7, 1, 6, 0, 0),
(34, 'Deportivo Sanarate vs Municipal', '2019-09-01 00:00:00', 5, 10, 17, 6, 0, 0),
(35, 'Cotzumalguapa vs Antigua GFC', '2019-09-01 00:00:00', 4, 1, 21, 6, 0, 0),
(36, 'Mixco vs Malacateco', '2019-09-01 00:00:00', 9, 8, 18, 6, 0, 0),
(37, 'Antigua GFC vs Siquinalá', '2019-09-14 00:00:00', 1, 11, 13, 7, 0, 0),
(38, 'Municipal vs Cobán Imperial', '2019-09-14 00:00:00', 10, 2, 20, 7, 0, 0),
(39, 'Malacateco vs Deportivo Sanarate', '2019-09-14 00:00:00', 8, 5, 7, 7, 0, 0),
(40, 'Xelajú vs Cotzumalguapa', '2019-09-14 00:00:00', 12, 4, 10, 7, 0, 0),
(41, 'Iztapa vs Mixco', '2019-09-15 00:00:00', 7, 9, 19, 7, 0, 0),
(42, 'Guastatoya vs Comunicaciones', '2019-09-15 00:00:00', 6, 3, 16, 7, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posicion`
--

DROP TABLE IF EXISTS `posicion`;
CREATE TABLE `posicion` (
  `idposicion` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELACIONES PARA LA TABLA `posicion`:
--

--
-- Volcado de datos para la tabla `posicion`
--

INSERT INTO `posicion` (`idposicion`, `descripcion`) VALUES
(1, 'Defensa'),
(2, 'Delantero'),
(3, 'Medio'),
(4, 'Portero');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificacion`
--
ALTER TABLE `calificacion`
  ADD PRIMARY KEY (`idcalificacion`);

--
-- Indices de la tabla `detalle_equipo`
--
ALTER TABLE `detalle_equipo`
  ADD PRIMARY KEY (`iddetalle_equipo`),
  ADD KEY `idequipo_idx` (`idequipo`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`idequipo`),
  ADD UNIQUE KEY `idequipo_UNIQUE` (`idequipo`),
  ADD KEY `fk_equipo_pais1_idx` (`idpais`);

--
-- Indices de la tabla `estadio`
--
ALTER TABLE `estadio`
  ADD PRIMARY KEY (`idestadio`);

--
-- Indices de la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`idJugador`),
  ADD UNIQUE KEY `idJugador_UNIQUE` (`idJugador`),
  ADD KEY `fk_Jugador_equipo1_idx` (`idequipo`),
  ADD KEY `fk_Jugador_posicion1_idx` (`idposicion`);

--
-- Indices de la tabla `jugador_has_calificacion`
--
ALTER TABLE `jugador_has_calificacion`
  ADD PRIMARY KEY (`iddetalle`),
  ADD UNIQUE KEY `iddetalle_UNIQUE` (`iddetalle`),
  ADD KEY `fk_jugador_has_calificacion_jugador1_idx` (`idJugador`),
  ADD KEY `fk_jugador_has_calificacion_calificacion1_idx` (`idcalificacion`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idpais`),
  ADD UNIQUE KEY `idpais_UNIQUE` (`idpais`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`idpartido`),
  ADD UNIQUE KEY `idpartido_UNIQUE` (`idpartido`),
  ADD KEY `fk_partido_equipo1_idx` (`equpo1`),
  ADD KEY `fk_partido_equipo2_idx` (`equpo2`),
  ADD KEY `fk_partido_estadio1_idx` (`idestadio`);

--
-- Indices de la tabla `posicion`
--
ALTER TABLE `posicion`
  ADD PRIMARY KEY (`idposicion`),
  ADD UNIQUE KEY `idposicion_UNIQUE` (`idposicion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `calificacion`
--
ALTER TABLE `calificacion`
  MODIFY `idcalificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `detalle_equipo`
--
ALTER TABLE `detalle_equipo`
  MODIFY `iddetalle_equipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `idequipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `estadio`
--
ALTER TABLE `estadio`
  MODIFY `idestadio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `jugador`
--
ALTER TABLE `jugador`
  MODIFY `idJugador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317;

--
-- AUTO_INCREMENT de la tabla `jugador_has_calificacion`
--
ALTER TABLE `jugador_has_calificacion`
  MODIFY `iddetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idpais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `idpartido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `posicion`
--
ALTER TABLE `posicion`
  MODIFY `idposicion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_equipo`
--
ALTER TABLE `detalle_equipo`
  ADD CONSTRAINT `idequipo` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`idequipo`);

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `fk_equipo_pais1` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idpais`);

--
-- Filtros para la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD CONSTRAINT `fk_Jugador_equipo1` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_Jugador_posicion1` FOREIGN KEY (`idposicion`) REFERENCES `posicion` (`idposicion`);

--
-- Filtros para la tabla `jugador_has_calificacion`
--
ALTER TABLE `jugador_has_calificacion`
  ADD CONSTRAINT `fk_jugador_has_calificacion_calificacion1` FOREIGN KEY (`idcalificacion`) REFERENCES `calificacion` (`idcalificacion`),
  ADD CONSTRAINT `fk_jugador_has_calificacion_jugador1` FOREIGN KEY (`idJugador`) REFERENCES `jugador` (`idJugador`);

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `fk_partido_equipo1` FOREIGN KEY (`equpo1`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_partido_equipo2` FOREIGN KEY (`equpo2`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_partido_estadio1` FOREIGN KEY (`idestadio`) REFERENCES `estadio` (`idestadio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
