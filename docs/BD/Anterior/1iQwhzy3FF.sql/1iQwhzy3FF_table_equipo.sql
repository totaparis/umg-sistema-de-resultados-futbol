
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

DROP TABLE IF EXISTS `equipo`;
CREATE TABLE IF NOT EXISTS `equipo` (
  `idequipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `idpais` int(11) NOT NULL,
  PRIMARY KEY (`idequipo`),
  UNIQUE KEY `idequipo_UNIQUE` (`idequipo`),
  KEY `fk_equipo_pais1_idx` (`idpais`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`idequipo`, `descripcion`, `idpais`) VALUES
(1, 'Antigua GFC', 1),
(2, 'Cobán Imperial', 1),
(3, 'Comunicaciones', 1),
(4, 'Cotzumalguapa', 1),
(5, 'Deportivo Sanarate', 1),
(6, 'Guastatoya', 1),
(7, 'Iztapa', 1),
(8, 'Malacateco', 1),
(9, 'Mixco', 1),
(10, 'Municipal', 1),
(11, 'Siquinalá', 1),
(12, 'Xelajú', 1);
