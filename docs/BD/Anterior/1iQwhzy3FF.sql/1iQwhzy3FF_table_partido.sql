
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

DROP TABLE IF EXISTS `partido`;
CREATE TABLE IF NOT EXISTS `partido` (
  `idpartido` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `equpo1` int(11) NOT NULL,
  `equpo2` int(11) NOT NULL,
  `idestadio` int(11) NOT NULL,
  `jornada` int(11) DEFAULT NULL,
  `resultado1` int(11) DEFAULT NULL,
  `resultado2` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartido`),
  UNIQUE KEY `idpartido_UNIQUE` (`idpartido`),
  KEY `fk_partido_equipo1_idx` (`equpo1`),
  KEY `fk_partido_equipo2_idx` (`equpo2`),
  KEY `fk_partido_estadio1_idx` (`idestadio`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`idpartido`, `nombre`, `fecha`, `equpo1`, `equpo2`, `idestadio`, `jornada`, `resultado1`, `resultado2`) VALUES
(1, 'Cotzumalguapa vs Sanarate', '2019-07-28 00:00:00', 4, 5, 21, 1, 0, 0),
(2, 'Siquinalá vs Mixco', '2019-07-27 00:00:00', 11, 9, 15, 1, 0, 0),
(3, 'Comunicaciones vs Cobán Imperial', '2019-07-27 00:00:00', 3, 2, 1, 1, 0, 0),
(4, 'Xelajú MC vs Municipal', '2019-07-27 00:00:00', 12, 10, 10, 1, 0, 0),
(5, 'Antigua GFC vs Malacateco', '2019-07-28 00:00:00', 1, 8, 13, 1, 0, 0),
(6, 'Guastatoya vs Iztapa', '2019-07-28 00:00:00', 6, 7, 16, 1, 0, 0),
(7, 'Deportivo Sanarate vs Siquinalá', '2019-08-03 00:00:00', 5, 11, 17, 2, 0, 0),
(8, 'Cobán Imperial vs Corzumalguapa', '2019-08-03 00:00:00', 2, 4, 5, 2, 0, 0),
(9, 'Municipal vs Comunicaciones', '2019-08-03 00:00:00', 10, 3, 1, 2, 0, 0),
(10, 'Malacateco vs Xelajú', '2019-08-03 00:00:00', 8, 12, 7, 2, 0, 0),
(11, 'Mixco vs Guastatoya', '2019-08-03 00:00:00', 9, 6, 18, 2, 0, 0),
(12, 'Iztapa vs Antigua GFC', '2019-08-03 00:00:00', 7, 1, 19, 2, 0, 0),
(13, 'Siquinalá vs Coban Imperial', '2019-08-09 00:00:00', 11, 2, 15, 3, 0, 0),
(14, 'Municipal vs Malacateco', '2019-08-09 00:00:00', 10, 8, 20, 3, 0, 0),
(15, 'Guastatoya vs Deportivo Sanarate', '2019-08-09 00:00:00', 6, 5, 16, 3, 0, 0),
(16, 'Xelajú vs Iztapa', '2019-08-14 00:00:00', 12, 7, 10, 3, 0, 0),
(17, 'Cotzumalguapa vs Comunicaciones', '2019-08-14 00:00:00', 4, 3, 21, 3, 0, 0),
(18, 'Antigua GFC vs Mixco', '2019-08-09 00:00:00', 1, 9, 13, 3, 0, 0),
(19, 'Iztapa vs Municipal', '2019-08-17 00:00:00', 7, 10, 19, 4, 0, 0),
(20, 'Comunicaciones vs Malacateco', '2019-08-17 00:00:00', 3, 8, 1, 4, 0, 0),
(21, 'Cobán Imperial vs Guastatoya', '2019-08-17 00:00:00', 2, 6, 5, 4, 0, 0),
(22, 'Deportivo Sanarate vs Antigua GFC', '2019-08-18 00:00:00', 5, 1, 17, 4, 0, 0),
(23, 'Cotzumalguapa vs Siquinalá', '2019-08-18 00:00:00', 4, 11, 21, 4, 0, 0),
(24, 'Mixco vs Xelajú', '2019-08-18 00:00:00', 9, 12, 18, 4, 0, 0),
(25, 'Siquinalá vs Comunicaciones', '2019-08-24 00:00:00', 11, 3, 15, 5, 0, 0),
(26, 'Municipal vs Mixco', '2019-08-24 00:00:00', 10, 9, 20, 5, 0, 0),
(27, 'Xelajú vs Deportivo Sanarate', '2019-08-24 00:00:00', 12, 5, 10, 5, 0, 0),
(28, 'Antigua GFC vs Cobán Imperial', '2019-08-25 00:00:00', 1, 2, 13, 5, 0, 0),
(29, 'Malacateco vs Iztapa', '2019-08-25 00:00:00', 8, 7, 7, 5, 0, 0),
(30, 'Guastatoya vs Cotzumalguapa', '2019-08-25 00:00:00', 6, 4, 16, 5, 0, 0),
(31, 'Siquinalá vs Guastatoya', '2019-08-31 00:00:00', 11, 6, 15, 6, 0, 0),
(32, 'Cobán Imperial vs Xelajú', '2019-08-31 00:00:00', 2, 12, 5, 6, 0, 0),
(33, 'Comuncaciones vs Iztapa', '2019-08-31 00:00:00', 3, 7, 1, 6, 0, 0),
(34, 'Deportivo Sanarate vs Municipal', '2019-09-01 00:00:00', 5, 10, 17, 6, 0, 0),
(35, 'Cotzumalguapa vs Antigua GFC', '2019-09-01 00:00:00', 4, 1, 21, 6, 0, 0),
(36, 'Mixco vs Malacateco', '2019-09-01 00:00:00', 9, 8, 18, 6, 0, 0),
(37, 'Antigua GFC vs Siquinalá', '2019-09-14 00:00:00', 1, 11, 13, 7, 0, 0),
(38, 'Municipal vs Cobán Imperial', '2019-09-14 00:00:00', 10, 2, 20, 7, 0, 0),
(39, 'Malacateco vs Deportivo Sanarate', '2019-09-14 00:00:00', 8, 5, 7, 7, 0, 0),
(40, 'Xelajú vs Cotzumalguapa', '2019-09-14 00:00:00', 12, 4, 10, 7, 0, 0),
(41, 'Iztapa vs Mixco', '2019-09-15 00:00:00', 7, 9, 19, 7, 0, 0),
(42, 'Guastatoya vs Comunicaciones', '2019-09-15 00:00:00', 6, 3, 16, 7, 0, 0);
