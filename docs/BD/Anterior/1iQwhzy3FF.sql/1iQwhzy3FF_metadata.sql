
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_equipo`
--
ALTER TABLE `detalle_equipo`
  ADD CONSTRAINT `idequipo` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`idequipo`);

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `fk_equipo_pais1` FOREIGN KEY (`idpais`) REFERENCES `pais` (`idpais`);

--
-- Filtros para la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD CONSTRAINT `fk_Jugador_equipo1` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_Jugador_posicion1` FOREIGN KEY (`idposicion`) REFERENCES `posicion` (`idposicion`);

--
-- Filtros para la tabla `jugador_has_calificacion`
--
ALTER TABLE `jugador_has_calificacion`
  ADD CONSTRAINT `fk_jugador_has_calificacion_calificacion1` FOREIGN KEY (`idcalificacion`) REFERENCES `calificacion` (`idcalificacion`),
  ADD CONSTRAINT `fk_jugador_has_calificacion_jugador1` FOREIGN KEY (`idJugador`) REFERENCES `jugador` (`idjugador`);

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `fk_partido_equipo1` FOREIGN KEY (`equpo1`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_partido_equipo2` FOREIGN KEY (`equpo2`) REFERENCES `equipo` (`idequipo`),
  ADD CONSTRAINT `fk_partido_estadio1` FOREIGN KEY (`idestadio`) REFERENCES `estadio` (`idestadio`);
