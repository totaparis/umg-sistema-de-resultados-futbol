<?php
header('Access-Control-Allow-Origin: *');  
//include('db.php');
 class controlador{

    private $mysqli;

    function connect(){
        //publico
         $this->mysqli = new mysqli("localhost", "id11013634_jesua", "jesua","id11013634_umgdb");
        //local
        // $mysqli = new mysqli("localhost", "usuario", "contraseña", "basedatos");
        //$this->mysqli = new mysqli("localhost", "root", "", "1iQwhzy3FF");
        if ($this->mysqli->connect_errno) {
            echo "Fallo al conectar a MySQL: " . $mysqli->connect_error;
        }else{
            //echo 'conecto';
        }
    }

    function estadisticas($setIdPartido){
        $this->connect();
        $resultado = $this->mysqli->query("
        select * from (
            SELECT 
        	de.iddetalle_equipo,
        	de.idequipo,
        	de.puntoS,
        	de.jugado,
        	de.ganado,
        	de.empate,
        	de.perdido,
        	de.afavor,
        	de.encontra,
        	de.remate,
        	de.remateaarco,
        	de.posesion,
        	de.pase,
        	de.presisionpase,
        	de.falta,
        	de.posisionadelantada,
        	de.tirosesquina,
        	de.jornada,
        	(SELECT COUNT(*)
        	FROM jugador j 
        	left JOIN jugador_has_calificacion jhc
        	ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
        	WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=1 AND p.jornada=de.jornada) Goles,
        	(SELECT COUNT(*)
        	FROM jugador j 
        	left JOIN jugador_has_calificacion jhc
        	ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
        	WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=2 AND p.jornada=de.jornada) TarjetaAmarilla,
            (SELECT COUNT(*)
        	FROM jugador j 
        	left JOIN jugador_has_calificacion jhc
        	ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
        	WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=3 AND p.jornada=de.jornada) TarjetaRoja,
        	(SELECT p.idpartido
            FROM partido p
            WHERE (de.idequipo=p.equpo1 AND de.jornada=p.jornada) OR (de.idequipo=p.equpo2 AND de.jornada=p.jornada)) idpartido,
            (SELECT p.fecha
            FROM partido p
            WHERE (de.idequipo=p.equpo1 AND de.jornada=p.jornada) OR (de.idequipo=p.equpo2 AND de.jornada=p.jornada)) fecha_patido
        FROM 
        	detalle_equipo de) t where idpartido = ".$setIdPartido."
    ");
        $jsonfinal = array();
        $i = 0;
        while ($fila = $resultado->fetch_assoc()) {
            $nombre_equipo = $this->mysqli->query(
             "SELECT * FROM equipo WHERE idequipo = '".$fila['idequipo']."'");
            $fila_equipo = $nombre_equipo->fetch_assoc();
            $goles = array();
            $jugador_gol = $this->mysqli->query(
                "SELECT nombre FROM jugador_has_calificacion jhc, jugador ju WHERE jhc.idcalificacion = 1 AND jhc.idpartido = '".$fila['idpartido']."' and ju.idJugador = jhc.idJugador");
                 while($row = $jugador_gol->fetch_assoc()){
                    $goles[] = utf8_encode($row['nombre']);
                 }
            $jsonfinal['data'][] = $fila;
            $jsonfinal['data'][$i]['nombreEquipo'] = utf8_encode($fila_equipo['descripcion']);
            $jsonfinal['data'][$i]['jugadorGol'] = (count($goles)>0) ? $goles : "";
            $jsonfinal['fechaDelPartido'] = $fila['fecha_patido'];
            $i++;
        }
        $resultado->free();
        //print_r($jsonfinal);
       return  json_encode($jsonfinal);
   
    }

    //siguiente pantalla    
   function partidos(){
        $this->connect();
        $resultado = $this->mysqli->query("SELECT distinct jornada FROM partido ");
        $jsonfinal = array();
        $jsonPartidos = array();
        $i=0;
        while ($fila = $resultado->fetch_assoc()) { 
           $jsonfinal['data'][] = $fila;                                         
        }
        $jsonPatidos = array();
        $jsonEquipo=array();
        $jsonPart = array();
        foreach($jsonfinal['data'] as $k=>$v){            
            $partido = $this->mysqli->query("select * from partido where jornada = '".$v['jornada']."' ");            
            while ($fila_partido = $partido->fetch_assoc()) {                   
                $equipoUno = $this->mysqli->query("select descripcion from equipo where idequipo = '".$fila_partido['equpo1']."' ");            
                $equipoDos = $this->mysqli->query("select descripcion from equipo where idequipo = '".$fila_partido['equpo2']."' ");            
                $fila_equipoUno = $equipoUno->fetch_assoc();
                //print_r($fila_partido);
                $fila_equipoDos = $equipoDos->fetch_assoc();
                $tPartidos[] = array(
                    'idPartido'=>$fila_partido['idpartido'],
                    'fecha'=> $fila_partido['fecha'],
                    'equipo'=> array(
                        array(
                            'nombreEquipo' => utf8_encode($fila_equipoUno['descripcion']),
                            'cantidadGol'=> $fila_partido['equpo1']
                        ),
                        array(
                            'nombreEquipo' => utf8_encode($fila_equipoDos['descripcion']),
                            'cantidadGol'=> $fila_partido['equpo2']
                        )
                    )
                );
            }      
            $jsonfinal['data'][$k]['partidos'] = $tPartidos; 
        }
        $resultado->free();
        return json_encode($jsonfinal);
    }


    function posiciones(){
        $this->connect();
        $resultado = $this->mysqli->query("
            SELECT * ,
            e.descripcion nombreEquipo,
            d.jugado pj,
            d.ganado g,
            d.empate e,
            d.perdido p,
            d.afavor gf,
            d.encontra gc,
            d.puntoS pts
            FROM detalle_equipo d, equipo e 
            where d.iddetalle_equipo = e.idequipo
            order by d.puntoS desc
        ");

        $jsonfinal = array();
        $i=1;
        while ($fila = $resultado->fetch_assoc()) {
           
            $jsonfinal['data'][] =array(
                'posicion'=> $i,
                'nombreEquipo' => utf8_encode($fila['nombreEquipo']),
                'pj' => $fila['pj'],
                'g' => $fila['g'],
                'e' => $fila['e'],
                'p' => $fila['p'],
                'gf' => $fila['gf'],
                'gc' => $fila['gc'],
                'pts' => $fila['pts']
            );
            $i++;
        }

        return json_encode($jsonfinal);

    }


}

?>
<?php

//include('db.php');
 class controlador{

    private $mysqli;

    function connect(){
        //publico
        //  $this->mysqli = new mysqli("localhost", "id11013634_jesua", "jesua","id11013634_umgdb");
        //local
        // $mysqli = new mysqli("localhost", "usuario", "contraseña", "basedatos");
        $this->mysqli = new mysqli("localhost", "root", "", "1iQwhzy3FF");
        if ($this->mysqli->connect_errno) {
            echo "Fallo al conectar a MySQL: " . $mysqli->connect_error;
        }else{
            //echo 'conecto';
        }
    }

    function estadisticas($setIdPartido){
        $this->connect();
        $resultado = $this->mysqli->query("
        select * from (
            SELECT 
            de.iddetalle_equipo,
            de.idequipo,
            de.puntoS,
            de.jugado,
            de.ganado,
            de.empate,
            de.perdido,
            de.afavor,
            de.encontra,
            de.remate,
            de.remateaarco,
            de.posesion,
            de.pase,
            de.presisionpase,
            de.falta,
            de.posisionadelantada,
            de.tirosesquina,
            de.jornada,
            (SELECT COUNT(*)
            FROM jugador j 
            left JOIN jugador_has_calificacion jhc
            ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
            WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=1 AND p.jornada=de.jornada) Goles,
            (SELECT COUNT(*)
            FROM jugador j 
            left JOIN jugador_has_calificacion jhc
            ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
            WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=2 AND p.jornada=de.jornada) TarjetaAmarilla,
            (SELECT COUNT(*)
            FROM jugador j 
            left JOIN jugador_has_calificacion jhc
            ON j.idJugador=jhc.idJugador
            left JOIN partido p
            ON p.idpartido=jhc.idpartido
            WHERE j.idequipo=de.idequipo AND jhc.idcalificacion=3 AND p.jornada=de.jornada) TarjetaRoja,
            (SELECT p.idpartido
            FROM partido p
            WHERE (de.idequipo=p.equpo1 AND de.jornada=p.jornada) OR (de.idequipo=p.equpo2 AND de.jornada=p.jornada)) idpartido,
            (SELECT p.fecha
            FROM partido p
            WHERE (de.idequipo=p.equpo1 AND de.jornada=p.jornada) OR (de.idequipo=p.equpo2 AND de.jornada=p.jornada)) fecha_patido
        FROM 
            detalle_equipo de) t where idpartido = ".$setIdPartido."
    ");
        $jsonfinal = array();
        $i = 0;
        while ($fila = $resultado->fetch_assoc()) {
            $nombre_equipo = $this->mysqli->query(
             "SELECT * FROM EQUIPO WHERE IDEQUIPO = '".$fila['idequipo']."'");
            $fila_equipo = $nombre_equipo->fetch_assoc();
            $goles = array();
            $jugador_gol = $this->mysqli->query(
                "SELECT nombre FROM jugador_has_calificacion jhc, jugador ju WHERE jhc.idcalificacion = 1 AND jhc.idpartido = '".$fila['idpartido']."' and ju.idJugador = jhc.idJugador");
                 while($row = $jugador_gol->fetch_assoc()){
                    $goles[] = utf8_encode($row['nombre']);
                 }
                //  json_decode(utf8_encode($json), true)
            $jsonfinal['data'][] = $fila;
            $jsonfinal['data'][$i]['nombreEquipo'] = utf8_encode($fila_equipo['descripcion']);
            $jsonfinal['data'][$i]['jugadorGol'] = (count($goles)>0) ? $goles : "";
            $jsonfinal['fechaDelPartido'] = $fila['fecha_patido'];
            $i++;
        }
        $resultado->free();
       return  json_encode($jsonfinal);
    }

    //siguiente pantalla    
   function partidos(){
        $this->connect();
        $resultado = $this->mysqli->query("SELECT distinct jornada FROM partido ");
        $jsonfinal = array();
        $jsonPartidos = array();
        $i=0;
        while ($fila = $resultado->fetch_assoc()) { 
           $jsonfinal['data'][] = $fila;                                         
        }
        $jsonPatidos = array();
        $jsonEquipo=array();
        $jsonPart = array();
        foreach($jsonfinal['data'] as $k=>$v){            
            $partido = $this->mysqli->query("select * from partido where jornada = '".$v['jornada']."' ");            
            while ($fila_partido = $partido->fetch_assoc()) {                   
                $equipoUno = $this->mysqli->query("select descripcion from equipo where idequipo = '".$fila_partido['equpo1']."' ");            
                $equipoDos = $this->mysqli->query("select descripcion from equipo where idequipo = '".$fila_partido['equpo2']."' ");            
                $fila_equipoUno = $equipoUno->fetch_assoc();
                $fila_equipoDos = $equipoDos->fetch_assoc();
                $tPartidos[] = array(
                    'idPartido'=>$fila_partido['idpartido'],
                    'fecha'=> $fila_partido['fecha'],
                    'equipo'=> array(
                        array(
                            'nombreEquipo' => utf8_encode($fila_equipoUno['descripcion']),
                            'cantidadGol'=> 2
                        ),
                        array(
                            'nombreEquipo' => utf8_encode($fila_equipoDos['descripcion']),
                            'cantidadGol'=> 2
                        )
                    )
                );
            }      
            $jsonfinal['data'][$k]['partidos'] = $tPartidos; 
        }
        $resultado->free();
        return json_encode($jsonfinal);
    }


    function posiciones(){
        $this->connect();
        $resultado = $this->mysqli->query("
            SELECT * ,
            e.descripcion nombreEquipo,
            d.jugado pj,
            d.ganado g,
            d.empate e,
            d.perdido p,
            d.afavor gf,
            d.encontra gc,
            d.puntoS pts
            FROM detalle_equipo d, equipo e 
            where d.iddetalle_equipo = e.idequipo
            order by d.puntoS desc
        ");

        $jsonfinal = array();
        $i=1;
        while ($fila = $resultado->fetch_assoc()) {
           
            $jsonfinal['data'][] =array(
                'posicion'=> $i,
                'nombreEquipo' => utf8_encode($fila['nombreEquipo']),
                'pj' => $fila['pj'],
                'g' => $fila['g'],
                'e' => $fila['e'],
                'p' => $fila['p'],
                'gf' => $fila['gf'],
                'gc' => $fila['gc'],
                'pts' => $fila['pts']
            );
            $i++;
        }

        return json_encode($jsonfinal);


    }

}

?>
