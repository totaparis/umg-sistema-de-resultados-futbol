<?php

/**
 * 
 Documentacion:
 Para poder acceder a las rutas se debe ingresar de la siguiente forma.
 
 Nombre ruta: estadisticas
 Ruta: umg-sistema-de-resultados-futbol/api/ruta.php?api=1&idPartido=1
 Variables:
 [
     api: llamada a la funcion,
     idPartido: id del partido
 ]

 Nombre ruta: partidos
 Ruta: umg-sistema-de-resultados-futbol/api/ruta.php?api=2
 Variables:
 [
     api: llamada a la funcion
 ]

 Z!M%VP*x)Y)%8sHJM3bf

 * 
 * 
 *  
 **/

    include('controlador.php');
    $json = $_GET['api'];

    $controlador = new controlador();
    
    if($json == 1){
        echo $controlador->estadisticas($_GET['idPartido']);
    }else if($json == 2){
        echo $controlador->partidos();
    }else if($json == 3){
        echo $controlador->posiciones();
    }


?>